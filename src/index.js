import React from 'react';
import ReactDOM from 'react-dom';

//bootstrap stylesheet
import 'bootstrap/dist/css/bootstrap.min.css';

import App from './App';
//import AppNavbar from "./AppNavbar";



ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')

);


/*const name = "John Smith"
const element = <h1>Hello, {name}</h1>

ReactDOM.render(
  element, document.getElementById('root')

);*/


/*
Mini Activity
  Create an object which contains properties firstName and lastName

  Using a function, join the firstName and lastName together and display it in the browser using ReactDom.render()

  Send screenshot in the group hangouts
*/

/*const user = {
  firstName: "Elito",
  lastName: "Penaranda"
}

function fullName(user){
  return `${user.firstName} ${user.lastName}`
}

const element = <h1>Hello, {fullName(user)}</h1>

ReactDOM.render(
  element, document.getElementById('root')

);*/
